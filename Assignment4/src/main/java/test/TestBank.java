package test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;
import model.*;

public class TestBank {

	@Test
	public void test() {
		
		Bank bank = new Bank();
		Set<Person> personSet = new HashSet<Person>();
		Person P1 = new Person(1, "A");
		Person P2 = new Person(2, "B");
		Person P3 = new Person(3, "C");
		bank.addPerson(P1); 
		personSet.add(P1);
		
		bank.addPerson(P2);
		personSet.add(P2);
		
		bank.addPerson(P3);
		personSet.add(P3);
		
		//check client addition
		assertEquals(bank.getBank().keySet(), personSet);
		
		bank.createSavingAccount(1, 2);
		bank.createSpendingAccount(1);
		bank.createSpendingAccount(2);
		bank.createSpendingAccount(3);
		
		//check account creation
		Iterator<Account> it = bank.getBank().get(P1).iterator();
		assertEquals(it.next().getClass().getSimpleName(), "SavingAccount");
		assertEquals(it.next().getClass().getSimpleName(), "SpendingAccount");
		it =  bank.getBank().get(P1).iterator();
		//check balance initialization
		assertEquals(new Double(it.next().getMoney()), new Double(0.0));
		assertEquals(new Double(it.next().getMoney()), new Double(0.0));
		
		try{
			bank.depositMoney(1, 2, 100);
			bank.depositMoney(1, 1, 5000);
			bank.depositMoney(2, 1, 300);
			bank.depositMoney(1, 2, 100);
		} catch(Exception e) {}
		it = bank.getBank().get(P1).iterator();
		
		//check money depositing
		assertEquals(new Double(it.next().getMoney()), new Double(5000));
		assertEquals(new Double(it.next().getMoney()), new Double(200));
		
		it = bank.getBank().get(P2).iterator();
		
		assertEquals(new Double(it.next().getMoney()), new Double(300));
		
		try {
			
			bank.withdrawMoney(1, 2, 60);
			bank.withdrawMoney(2, 1, 50);
		} catch(Exception e) {}
		
		//check money withdrawal
		it = bank.getBank().get(P1).iterator();
		it.next();
		assertEquals(new Double(it.next().getMoney()), new Double(140));
		it = bank.getBank().get(P2).iterator();
		assertEquals(new Double(it.next().getMoney()), new Double(250));
		
		//check person editing
		bank.editPerson(1, new Person(1, "A A"));
		assertEquals(bank.getPersonWithId(1).getName(), "A A");
		
		//check person deleting
		bank.deletePerson(bank.getPersonWithId(1));
		assertEquals(bank.getPersonWithId(1), null);
		
		//check account removal
		bank.removeAccount(2, 1);
		
		assertEquals(bank.getBank().get(P2).size(), 0);
	}

}
