package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.event.TableModelListener;

import model.Bank;
import model.Person;

public class View extends JFrame {

	
	private Bank bank;
	
	//Menu Frame
	private JButton openClientMenu = new JButton("  Open Client Menu  ");
	private JButton viewAccountMenu = new JButton("  View Accounts  ");
	
	//Client Frame
	private JFrame clientFrame = new JFrame();
	private JPanel clientPanel = new JPanel();
	private JTable clientTable = new JTable();
	private JButton addNewClient = new JButton("                                                      New Client                                                  ");
	
	//Account Frame
	private JFrame accountFrame = new JFrame();
	private JPanel accountPanel = new JPanel();
	private JTable accountTable = new JTable();
	
	//Client Form
	private JFrame clientFormFrame = new JFrame();
	private JTextField nameField = new JTextField(10);
	private JButton addClient = new JButton("Add Client");
	
	//Personal Client Details
	private JPanel finalPanel = new JPanel();
	private JFrame personalFrame = new JFrame();
	private JPanel personInfo = new JPanel();
	private JLabel personalName = new JLabel("Name:");
	private JTextField personalNameField = new JTextField(20);
	
	private JPanel tableInfo = new JPanel();
	private JTable personalTable = new JTable();
	
	private JPanel clientOperations = new JPanel();
	private JButton personalDelete = new JButton("Delete Client");
	private JButton personalEdit = new JButton("Edit Client");
	
	private JPanel accountOperations = new JPanel();
	private JButton deleteAccount = new JButton("Delete Account");
	private JButton createAccount = new JButton("Create Account");
	private JCheckBox savingOrSpending = new JCheckBox();
	private JTextField interestField = new JTextField(10);
	private JButton depositMoney = new JButton("Deposit Money");
	private JButton withdrawMoney = new JButton("Withdraw Money");
	private JTextField amountField = new JTextField(10);
	
	//Person Working
	private Person personWorking;
	public View() {
		
		bank = (new Bank()).deserializeBank();
		this.setLayout(null);
		this.setSize(500, 200);
		this.setTitle("Bank Management");
		JPanel menuPanel = new JPanel();
		menuPanel.setSize(new Dimension(500,300));
		JLabel titleLabel = new JLabel("Bank Management Application");
		titleLabel.setBounds(new Rectangle(500, 100));
		titleLabel.setFont(new Font("Serif", Font.PLAIN, 35));
		menuPanel.add(titleLabel);
		viewAccountMenu.setBounds(200, 400, 400, 100);
		viewAccountMenu.setFont(new Font("Serif", Font.PLAIN, 20));
		viewAccountMenu.setBackground(new Color(200,100,50));
		openClientMenu.setBounds(300, 200, 400, 100);
		openClientMenu.setFont(new Font("Serif", Font.PLAIN, 20));
		openClientMenu.setBackground(new Color(200, 50, 80));
		menuPanel.add(openClientMenu);
		menuPanel.add(viewAccountMenu);
		this.add(menuPanel);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//Client Frame
		clientFrame.setSize(800, 400);
		clientFrame.setLayout(new GridLayout(2,2));
		clientFrame.setTitle("Client Menu");
		clientPanel.setLayout(new BorderLayout());
		clientTable = bank.createPersonTable();
		clientTable.setCellSelectionEnabled(false);
		clientTable.setRowSelectionAllowed(false);
		clientTable.setColumnSelectionAllowed(false);
	
		JScrollPane scroll = new JScrollPane(clientTable);
		clientPanel.add(scroll);
		JPanel buttonPanel = new JPanel();
		addNewClient.setFont(new Font("Serif", Font.PLAIN, 20));
		buttonPanel.add(addNewClient);
		clientFrame.add(clientPanel);
		clientFrame.add(buttonPanel);
		
		//Client Form
		clientFormFrame.setSize(400, 200);
		clientFormFrame.setTitle("Client Form");
		clientFormFrame.setLayout(new FlowLayout());
		JPanel clientFormPanel = new JPanel();
		clientFormPanel.setLayout(new BoxLayout(clientFormPanel, EXIT_ON_CLOSE));
		clientFormPanel.add(new JLabel("Name:"));
		clientFormPanel.add(nameField);
		addClient.setFont(new Font("Serif", Font.PLAIN, 40));
		clientFormPanel.add(addClient);
		clientFormFrame.add(clientFormPanel);
		
		//Account Frame
		accountFrame.setSize(800, 400);
		accountFrame.setLayout(new GridLayout(2,2));
		accountFrame.setTitle("AccountTable");
		accountPanel.setLayout(new BorderLayout());
		accountTable = bank.createAccountTable();
		JScrollPane scroll2 = new JScrollPane(accountTable);
		accountPanel.add(scroll2);
		accountFrame.add(accountPanel);
		
		//Personal Client Frame
	    personalFrame.setSize(900,700);
	    personalFrame.setLayout(new FlowLayout());
	
	}
	
	public void openPersonalFrame(int personId) {

		personalFrame.remove(finalPanel);
		
		personWorking = bank.getPersonWithId(personId);
		
		finalPanel.removeAll();
		personInfo.removeAll();
		tableInfo.removeAll();
		clientOperations.removeAll();
		accountOperations.removeAll();
		finalPanel.revalidate();
		finalPanel.setLayout(new BoxLayout(finalPanel, BoxLayout.Y_AXIS));
		personInfo.setLayout(new GridLayout(1,1));
		personInfo.add(personalName);
	    personInfo.add(personalNameField);
	    personalNameField.setFont(new Font("Serif", Font.PLAIN, 20));
	    personalNameField.setText(bank.getPersonWithId(personId).getName());
	    
	    finalPanel.add(personInfo);
	    personInfo.setBounds(100,100, 700, 100);
	    
	    tableInfo.setLayout(new BorderLayout());
	    personalTable = bank.createPersonalAccountTable(personId);
	    tableInfo.add(new JScrollPane(personalTable));
	    finalPanel.add(tableInfo);
	    
	    clientOperations.setLayout(new FlowLayout());
	    personalDelete.setFont(new Font("Serif", Font.PLAIN, 20));
	    personalEdit.setFont(new Font("Serif", Font.PLAIN, 20));
	    clientOperations.add(personalDelete);
	    clientOperations.add(personalEdit);
	    finalPanel.add(clientOperations);
	    
	    accountOperations.setLayout(new FlowLayout());
	    accountOperations.add(createAccount);
	    accountOperations.add(new JLabel("Saving"));
	    accountOperations.add(savingOrSpending);
	    accountOperations.add(new JLabel("Interest"));
	    accountOperations.add(interestField);
	    accountOperations.add(deleteAccount);
	
	   JPanel moneyPanel = new JPanel();
	   	moneyPanel.add(depositMoney);
	    moneyPanel.add(withdrawMoney);
	    moneyPanel.add(new JLabel("Amount"));
	    moneyPanel.add(amountField);
	    finalPanel.add(accountOperations);
	    finalPanel.add(moneyPanel);
	    finalPanel.repaint();
	    personalFrame.add(finalPanel);
	    personalFrame.setVisible(true);
	    personalFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	public Bank getBank() {
		
		return bank;
	}
	
	public void closePersonalFrame() {
		
		personalFrame.setVisible(false);
	}
	public Person getPersonWorking() {
		
		return personWorking;
	}
	public String getNewClientName() {
		
		return nameField.getText();
	}
	public void setClientTable(JTable table) {
		
		clientPanel.removeAll();
		clientTable = table;
		clientPanel.add(new JScrollPane(clientTable));
		clientTable.setCellSelectionEnabled(false);
		clientTable.setRowSelectionAllowed(false);
		clientTable.setColumnSelectionAllowed(false);
		clientPanel.revalidate();
		clientPanel.repaint();
	}
	
	public void setAccountTable(JTable table) {
		
		accountPanel.removeAll();
		accountTable = table;
		accountPanel.add(new JScrollPane(accountTable));
		accountPanel.revalidate();
		accountPanel.repaint();
	}
	
	public void setPersonalAccountTable(JTable table) {
		
		tableInfo.removeAll();
		personalTable = table;
		tableInfo.add(new JScrollPane(personalTable));
		tableInfo.revalidate();
		tableInfo.repaint();
	}
	
	public boolean checkBoxState() {
		
		return savingOrSpending.isSelected();
	}
	
	public int getInterest() {
		
		return Integer.parseInt(interestField.getText());
	}
	public JTable getClientTable() {
		
		return clientTable;
	}
	public void setClientFrameVisible() {
		
		clientFrame.setVisible(true);
	}
	
	public void setClientFormVisible() {
		
		clientFormFrame.setVisible(true);
	}
	
	public String getNewEditedName() {
		
		return personalNameField.getText();
	}
	
	public JTable getPersonlTable() {
		
		return personalTable;
	}
	
	public double getSum() {
		
		return Double.parseDouble(amountField.getText());
	}
	public void addClientMenuListener(ActionListener buttonPressed) {
		
		openClientMenu.addActionListener(buttonPressed);
	}
	
	public void setAccountFrameVisible() {
		
		accountFrame.setVisible(true);
	}
	
	public void addTableModelListener(TableModelListener listener) {
		
		clientTable.getModel().addTableModelListener(listener);
	}
	public void addAccountTableListener(ActionListener buttonPressed) {
		
		viewAccountMenu.addActionListener(buttonPressed);
	}
	
	
	public void addClientFormListener(ActionListener buttonPressed) {
		
		addNewClient.addActionListener(buttonPressed);
	}
	
	public void addNewClientListener(ActionListener buttonPressed) {
		
		addClient.addActionListener(buttonPressed);
	}
	
	public void deleteClientListener(ActionListener buttonPressed) {
		
		personalDelete.addActionListener(buttonPressed);
	}
	
	public void addEditClientListener(ActionListener buttonPressed) {
		
		personalEdit.addActionListener(buttonPressed);
	}
	
	public void addCreateAccountListener(ActionListener buttonPressed) {
		
		createAccount.addActionListener(buttonPressed);
	}
	
	public void addDeleteAccountListener(ActionListener buttonPressed) {
		
		deleteAccount.addActionListener(buttonPressed);
	}
	
	public void addDepositMoneyListener(ActionListener buttonPressed) {
		
		depositMoney.addActionListener(buttonPressed);
	}
	
	public void addWithdrawMoneyListener(ActionListener buttonPressed) {
		
		withdrawMoney.addActionListener(buttonPressed);
	}
	
	void showError(String errMessage) {
		
		JOptionPane.showMessageDialog(this, errMessage); //display a message dialog that states that an error happened 
	}
	
	void showMessage(String message) {
		
		JOptionPane.showMessageDialog(personalFrame, message);
	}
}
