package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import model.Person;

public class Controller {

	private View view;
	
	public Controller(View myView) {
		
		this.view = myView;
		view.addClientMenuListener(new ClientMenuListener());
		view.addAccountTableListener(new AccountTableListener());
		view.addClientFormListener(new ClientFormListener());
		view.addTableModelListener(new ClientTableListener());
		view.addNewClientListener(new NewClientListener());
		view.deleteClientListener(new DeleteClientListener());
		view.addEditClientListener(new EditClientListener());
		view.addCreateAccountListener(new CreateAccountListener());
		view.addDeleteAccountListener(new DeleteAccountListener());
		view.addDepositMoneyListener(new DepositMoneyListener());
		view.addWithdrawMoneyListener(new WithdrawMoneyListener());
	}
	
	public class ClientMenuListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			view.setClientFrameVisible();
	}
	}
	
	public class AccountTableListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			view.setAccountFrameVisible();
	}	
	}
	
	public class ClientFormListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			view.setClientFormVisible();
	}	
	}
	public class ClientTableListener implements TableModelListener {
		
		public void tableChanged(TableModelEvent e) {
			
			int selectedRow = view.getClientTable().getSelectedRow();
			int personId = Integer.parseInt(view.getClientTable().getValueAt(selectedRow, 0).toString());
			view.openPersonalFrame(personId);
		}
	}
	
	public class NewClientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			String newClient = view.getNewClientName();
			Set<Person> allPersons = view.getBank().getBank().keySet();
			int id=0;
			for(Person currentPerson: allPersons) {
				
				if(currentPerson.getId()>id){
					
					id = currentPerson.getId();
				}
			}
			id++;
			view.getBank().addPerson(new Person(id, newClient));
			view.setClientTable(view.getBank().createPersonTable());
			view.getBank().serializeBank();
			view.addTableModelListener(new ClientTableListener());
		}
	}
	
	public class DeleteClientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			view.getBank().deletePerson(view.getPersonWorking());
			view.setClientTable(view.getBank().createPersonTable());
			view.setAccountTable(view.getBank().createAccountTable());
			view.getBank().serializeBank();
			view.addTableModelListener(new ClientTableListener());
			view.closePersonalFrame();
		}
	}
	
	public class EditClientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			view.getBank().editPerson(view.getPersonWorking().getId(), new Person(view.getPersonWorking().getId(), view.getNewEditedName()));
			view.setClientTable(view.getBank().createPersonTable());
			view.setAccountTable(view.getBank().createAccountTable());
			view.getBank().serializeBank();
			view.addTableModelListener(new ClientTableListener());
		}
		}
	
	public class CreateAccountListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			if(view.checkBoxState()==false) {
				
				view.getBank().createSpendingAccount(view.getPersonWorking().getId());
			} else {
				
				view.getBank().createSavingAccount(view.getPersonWorking().getId(), view.getInterest());
			}
			view.getBank().serializeBank();
			view.setAccountTable(view.getBank().createAccountTable());
			view.setPersonalAccountTable(view.getBank().createPersonalAccountTable(view.getPersonWorking().getId()));
		}		
	}
	
	public class DeleteAccountListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			int selectedRow = view.getPersonlTable().getSelectedRow();
			view.getBank().removeAccount(view.getPersonWorking().getId(), Integer.parseInt(view.getPersonlTable().getValueAt(selectedRow, 0).toString()));
			view.getBank().serializeBank();
			view.setAccountTable(view.getBank().createAccountTable());
			view.setPersonalAccountTable(view.getBank().createPersonalAccountTable(view.getPersonWorking().getId()));
		}		
	}
	
	public class DepositMoneyListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			int selectedRow = 0;
			try{
			selectedRow = view.getPersonlTable().getSelectedRow();
			try{
			view.getBank().depositMoney(view.getPersonWorking().getId(), Integer.parseInt(view.getPersonlTable().getValueAt(selectedRow, 0).toString()), view.getSum());
			} catch(Exception exc) {
				
				view.showMessage(exc.getMessage());
			}
			view.getBank().serializeBank();
			view.setAccountTable(view.getBank().createAccountTable());
			view.setPersonalAccountTable(view.getBank().createPersonalAccountTable(view.getPersonWorking().getId()));
			} catch(Exception exc) {
				
				view.showMessage("You need to select an account!");
			}
		}		
	}
	
	public class WithdrawMoneyListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			try{
			int selectedRow = view.getPersonlTable().getSelectedRow();
			try{
			double withDrawn = view.getBank().withdrawMoney(view.getPersonWorking().getId(), Integer.parseInt(view.getPersonlTable().getValueAt(selectedRow, 0).toString()), view.getSum());
			view.showMessage("Withdrawn Money: " + withDrawn);
			} catch(Exception exc) {
				
				view.showMessage(exc.getMessage());
			}
			view.getBank().serializeBank();
			view.setAccountTable(view.getBank().createAccountTable());
			view.setPersonalAccountTable(view.getBank().createPersonalAccountTable(view.getPersonWorking().getId()));
			} catch(Exception exc) {
				
				view.showMessage("You need to select an account!");
			}
		}		
	}
}
