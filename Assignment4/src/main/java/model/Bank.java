package model;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observable;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Class that implements the BankProc and other methods that communicate with the GUI.
 * @version 1.1
 * @author rares
 * @since 5/10/2017
 */

public class Bank extends Observable implements java.io.Serializable, BankProc {

	private HashMap<Person, Set<Account>> bank;
	private Person currentObserver;
	
	public Bank() {
		
		bank = new HashMap<Person, Set<Account>>();
		currentObserver = null;
	}
	
	public HashMap<Person, Set<Account>> getBank() {
		
		return bank;
	}
	
	/**
	 * Adds a new person in the hash map
	 * @param P - the person to be added in the hash table with null acounts
	 * @pre P.getId()>=1
	 * @post @nochange
	 * @invariant isWellFormed()
	 */
	public void addPerson(Person P) {
		
		assert P.getId()>=1;
		
		bank.put(P, null);
		setChanged();
		this.deleteObserver(getPersonWithId(P.getId()));
	}
	
	/**
	 * Deletes a person from the hash map.
	 * @param P - the person to be deleted from the hash map.
	 * @pre bank.get(P)!=null
	 * @post bank.get(P)==null
	 * @invariant isWellFormed()
	 */
	public void deletePerson(Person P) {
		
		assert bank.get(P)!=null;
		
		bank.remove(P);
		
		assert bank.get(P)==null;
	}
	
	/**
	 * Updates the details of an already existent person
	 * @param id - The id of the person to be updated.
	 * @param P - the new details of the person.
	 * @pre bank.get(P)!=null&&P.getId()==id
	 * @post bank.get(P)!=null
	 */
	public void editPerson(int id, Person P) {
		
		assert getPersonWithId(id)!=null&&P.getId()==id;
		
		Set<Account> accounts = bank.get(getPersonWithId(id));
		bank.remove(getPersonWithId(id));
		bank.put(P, accounts);
		
		assert getPersonWithId(id)!=null;
	}
	
	/**
	 * Creates a table filled with the data of each person on each row.
	 * @return Returns the JTable with all persons.
	 */
	public JTable createPersonTable() {
		
		JTable finalTable = new JTable();
		int rows =bank.keySet().size();
		
		if(rows==0) {
			
			return finalTable;
		}
		String[] columns = {"Id", "Name"};
		String[][] tableData = new String[rows][2];
		int i=0;
		for(Person currentPerson: bank.keySet()) {
			String[] newRow = new String[2];
			newRow[0] = Integer.toString(currentPerson.getId());
			newRow[1] = currentPerson.getName();
			tableData[i] = newRow;
			i++;
		}
		
		DefaultTableModel model = new DefaultTableModel(tableData, columns);
		finalTable = new JTable(model);
		return finalTable;
	}
	
	/**
	 * Creates a table filled with the accounts of the give person.
	 * @param personId Represents the id of the person to have its accounts displayed.
	 * @return Returns the table filled with the accounts for which the person is the main holder.
	 */
	public JTable createPersonalAccountTable(int personId) {
		
		JTable finalTable = new JTable();
		Person P1 = getPersonWithId(personId);
		String[] columns = {"AccountId", "Amount", "DateModified", "Type", "Interest Time"};
		if(bank.get(P1)==null)
			return finalTable;
		int rows = bank.get(P1).size();
		
		String[][] tableData = new String[rows][5];
		int i=0;
		for(Account currentAccount: bank.get(P1)) {
			
			String oneRow[] = new String[5];
			oneRow[0] = Integer.toString(currentAccount.getId());
			oneRow[1] = Double.toString(currentAccount.getMoney());
			oneRow[2] = Integer.toString(currentAccount.getTime().getDay()) + "/" + Integer.toString(currentAccount.getTime().getMonth()) + 
					"/" + Integer.toString(currentAccount.getTime().getYear()) + " " + Integer.toString(currentAccount.getTime().getHour()) + ":" + Integer.toString(currentAccount.getTime().getMinutes());
			if(currentAccount.getClass().getSimpleName().toString().compareTo("SavingAccount")==0) {
				oneRow[3] = "Saving";
				oneRow[4] = Integer.toString(currentAccount.getInterest());
			} else {
				oneRow[3] = "Spending";
				oneRow[4] = new String("none");
			}
			tableData[i] = oneRow;
			i++;
		}
		DefaultTableModel model = new DefaultTableModel(tableData, columns);
		finalTable = new JTable(model);
		return finalTable;
		
	}
	
	/**
	 * Creates a table with all the accounts in the bank.
	 * @return Returns a JTable filled with all the accounts present in the bank.
	 */
	public JTable createAccountTable() {
		
		JTable finalTable = new JTable();
		int rows = 0;
		String[] columns = {"Person", "AccountId", "Amount", "DateModified", "Type", "Interest Period"};
		String[][] tableData;
		for(Person currentPerson: bank.keySet()) {
			if(bank.get(currentPerson)!=null)
			rows = rows + bank.get(currentPerson).size();
		}
		tableData = new String[rows][6];
		
		int i=0;
		for(Person currentPerson: bank.keySet()) {
			if(bank.get(currentPerson)!=null){
			for(Account currentAccount : bank.get(currentPerson)) {
				String oneRow[] = new String[6];
				oneRow[0] = currentPerson.getName();
				oneRow[1] = Integer.toString(currentAccount.getId());
				oneRow[2] = Double.toString(currentAccount.getMoney());
				oneRow[3] = Integer.toString(currentAccount.getTime().getDay()) + "/" + Integer.toString(currentAccount.getTime().getMonth()) + 
						"/" + Integer.toString(currentAccount.getTime().getYear()) + " " + Integer.toString(currentAccount.getTime().getHour()) + ":" + Integer.toString(currentAccount.getTime().getMinutes());
				if(currentAccount.getClass().getSimpleName().toString().compareTo("SavingAccount")==0) {
					oneRow[4] = "Saving";
					oneRow[5] = Integer.toString(currentAccount.getInterest());
				} else {
					oneRow[4] = "Spending";
					oneRow[5] = new String("none");
				}
				tableData[i] = oneRow;
				i++;
			}
			}
		}
		DefaultTableModel model = new DefaultTableModel(tableData, columns);
		finalTable = new JTable(model);
		return finalTable;
		
	}
	
	/**
	 * Creates a spending account for the person required by the id.
	 * @param personId Represent the id of the person that wants to create the spending account.
	 * @pre getPersonWithId(personId)!=null
	 * @post bank.get(getPersonWithId(personId)).size() > 0
	 * @invariant isWellFormed()
	 */
	public void createSpendingAccount(int personId) {
		
		assert getPersonWithId(personId)!=null;
		
		this.addObserver(getPersonWithId(personId));
		this.setChanged();
		this.notifyObservers("You have been created a spending account!");
		this.deleteObserver(getPersonWithId(personId));
		if(bank.get(getPersonWithId(personId))==null) {
			
			Set<Account> accounts = new HashSet<Account>();
			accounts.add(new SpendingAccount(1));
			bank.put(getPersonWithId(personId), accounts);
		} else {
			
			bank.get(getPersonWithId(personId)).add(new SpendingAccount(bank.get(getPersonWithId(personId)).size()+1));
		}
		assert bank.get(getPersonWithId(personId)).size() > 0;
	}
	
	/**
	 * Creates a saving account for the person required by the id.
	 * @param personId Represent the id of the person that wants to create the saving account.
	 * @pre getPersonWithId(personId)!=null&&interestTime>=1
	 * @post bank.get(getPersonWithId(personId)).size() > 0
	 * @invariant isWellFormed()
	 */
	public void createSavingAccount(int personId, int interestTime) {
		
		assert getPersonWithId(personId)!=null&&interestTime>=1;
		
		this.addObserver(getPersonWithId(personId));
		setChanged();
		this.notifyObservers("You have been created a saving account!");
		this.deleteObservers();
		if(bank.get(getPersonWithId(personId))==null) {
			
			Set<Account> accounts = new HashSet<Account>();
			accounts.add(new SavingAccount(1, interestTime));
			bank.put(getPersonWithId(personId), accounts);
		} else {
			
			bank.get(getPersonWithId(personId)).add(new SavingAccount(bank.get(getPersonWithId(personId)).size()+1,interestTime));
		}
		
		assert bank.get(getPersonWithId(personId)).size() > 0;
	}
	
	
	/**
	 * This method is used for suming the current amount in the account with an added sum.
	 * @param personId The id of the person which wants to deposit money
	 * @param accountId The id of the account in which the money should be added.
	 * @param sum The sum to be added needs to be positive.
	 * @pre sum>0&&getPersonWithId(personId)!=null
	 * @post A.getMoney()>=sum
	 * @invariant isWellFormed()
	 */
	public void depositMoney(int personId, int accountId, double sum) throws Exception {
		
		assert sum>0&&getPersonWithId(personId)!=null;
		
		Iterator<Account> it =  bank.get(getPersonWithId(personId)).iterator();
		Account A = null;
		while(it.hasNext()) {
			
			A = it.next();
			if(A.getId() ==accountId) {
				
				A.depositMoney(sum);
				break;
			}
		}
		this.addObserver(getPersonWithId(personId));
		setChanged();
		this.notifyObservers("Account nr. " + accountId + " has the new sum of " + A.getMoney());
		this.deleteObserver(getPersonWithId(personId));
		
		assert A.getMoney()>=sum;
	}
	
	/**
	 * This method is used for subtracting the current amount in the account with a sum.
	 * @param personId The id of the person which wants to withdraw money
	 * @param accountId The id of the account from which the money should be subtracted.
	 * @param sum The sum to be withdrawn needs to be positive.
	 * @pre sum>0&&getPersonWithId(personId)!=null
	 * @invariant isWellFormed()
	 */
	public double withdrawMoney(int personId, int accountId, double sum) throws Exception {
		
		//assert getPersonWithId(personId)!=null&&bank.get(getPersonWithId(personId))!=null&&sum>0;
		
		Iterator<Account> it =  bank.get(getPersonWithId(personId)).iterator();
		Account A = null;
		while(it.hasNext()) {
			
			A = it.next();
			if(A.getId() ==accountId) {
				
				A.withdrawMoney(sum);
				break;
			}
		}
		updateObserver(personId);
		setChanged();
		this.notifyObservers("Account nr. " + accountId + "has the new sum of " + A.getMoney());
		this.deleteObserver(getPersonWithId(personId));
		return A.getWithdrawnMoney();
	}
	
	
	/**
	 * Updates the current observer in the bank
	 * @param personId The id of the person which is the current observer.
	 */
	private void updateObserver(int personId) {
		
		currentObserver = getPersonWithId(personId);
		this.addObserver(currentObserver);
		
	}
	

	/**
	 * Method that returns the person with a give id.
	 * @param id The id of the person to be returned.
	 * @return The person with the required id.
	 */
	public Person getPersonWithId(int id) {
		
		for(Person currentPerson: bank.keySet()) {
			if(currentPerson.getId()==id){
				return currentPerson;
			}
		}
		return null;
	}
	
	/**
	 * Method that removes an account from the bank.
	 * @param personId Represents the id of the main holder of the account.
	 * @param accountId Represent the id of the account that needs to be removed.
	 * @pre getPersonWithId(personId)!=null
	 * @invariant isWellFormed()
	 */
	public void removeAccount(int personId, int accountId) {
		
		assert getPersonWithId(personId)!=null;
		
		Set<Account> accounts = bank.get(getPersonWithId(personId));
		Person P = getPersonWithId(personId);
		Iterator<Account> it = accounts.iterator();
		while(it.hasNext()) {
			
			Account A = it.next();
			if(A.getId()==accountId) {
				
				accounts.remove(A);
				break;
			}
		}
		bank.remove(getPersonWithId(personId));
		bank.put(P, accounts);
	}
	public void printBank() {
		
		for(Person currentPerson: bank.keySet()) {
			System.out.println(currentPerson.toString());
			if(bank.get(currentPerson)!=null)
			for(Account currentAccount: bank.get(currentPerson)) {
				System.out.println(currentAccount.toString());
			}
		}
	}
	
	
	/**
	 * Writes the bank in a given file using serialization,
	 */
	public void serializeBank() {
		
		try{
        FileOutputStream fileOut = new FileOutputStream("C:/Users/rares/Documents/bank.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(this);
        out.close();
        fileOut.close();
		} catch(Exception e) {
			
			System.out.println(e.getMessage() +  "aici" + e.getClass());
		}
	}
	
	/**
	 * Reads the bank object from the given file.
	 * @return Returns the bank object found in the serialized file.
	 */
	public Bank deserializeBank() {
		
		Bank b = new Bank();
		try {
			
			FileInputStream fileIn = new FileInputStream("C:/Users/rares/Documents/bank.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         b = (Bank) in.readObject();
	         in.close();
	         fileIn.close();
		} catch(Exception e) {
			
			System.out.println(e.getMessage());
		}
		return b;
	}
	
	protected boolean isWellFormed() {
		Set<Account> allAccounts = new HashSet<Account>();
		int i=0;
		for(Person currentPerson: bank.keySet()) {
			
			for(Account currentAccount: bank.get(currentPerson)) {
				
				allAccounts.add(currentAccount);
				i++;
			}
		}
		return allAccounts.size()==i;
}
}
