package model;

public interface BankProc {

	
	public void addPerson(Person P);
	
	public void deletePerson(Person P);
	
	public void editPerson(int id, Person P);
	
	public void createSpendingAccount(int personId);
	
	public void createSavingAccount(int personId, int interestTime);
	
	public double withdrawMoney(int personId, int accountId, double sum) throws Exception;
	
	public void depositMoney(int personId, int accountId, double sum) throws Exception;
}