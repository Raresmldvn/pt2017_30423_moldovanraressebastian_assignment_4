package model;

import java.util.Observable;
import java.util.Observer;

/**
 * Class that models the real world person.
 * @author rares
 * @version 1.0
 */
public class Person implements Observer, java.io.Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	
	public Person() {
		
		this.id = 0;
		this.name = "";
	}
	
	public Person(int personId, String personName) {
		
		this.id = personId;
		this.name = personName;
	}
	
	public int getId() {
		
		return id;
	}
	public String getName() {
		
		return name;
	}
	
	public void setId(int personId){
		
		this.id = personId;
	}
	
	public void setName(String personName) {
		
		this.name = personName;
	}
	
	public String toString() {
		
		return Integer.toString(id) + " " + name;
	}

	/**
	 * Method for updating when the person is an observer.
	 * @param arg0 Predefined parameter for the update function
	 * @param arg1 The string to be displayed.
	 */
	public void update(Observable arg0, Object arg1) {
		System.out.println(Integer.toString(this.getId()) + " " +  this.getName() + ": " + arg1.toString()); 
		
	}
}
