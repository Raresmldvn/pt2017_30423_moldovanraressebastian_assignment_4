package model;

public abstract class Account implements java.io.Serializable {

	private int id;
	private double money;
	private Time time;
	
	public Account() {
		
		this.id = 0;
		this.money = 0;
		this.time = new Time();
	}
	
	public Account(int accountId, double accountMoney, Time accountTime) {
		
		this.id = accountId;
		this.money = accountMoney;
		this.time = accountTime;
	}
	
	public int getId() {
		
		return id;
	}
	
	public double getMoney() {
		
		return money;
	}

	public Time getTime() {
		
		return time;
	}
	
	public void setId(int accountId) {
		
		this.id = accountId;
	}
	
	public void setMoney(double accountMoney) {
		
		this.money = accountMoney;
	}
	
	public void setTime(Time accountTime) {
		
		this.time = accountTime;
	}
	
	public String toString() {
		
		return "Account: " + id + ", sum: " + money + ", date: "  +  time.getDay() + "/" + time.getMonth() + "/" + time.getYear() + ", hour " + time.getHour() + ":" + time.getMinutes();
	}
	
	public int hashCode() {
		
		return id;
	}
	
	public abstract int getInterest();
	
	public abstract double getWithdrawnMoney();
	
	public abstract void depositMoney(double newSum) throws Exception;
	
	public abstract void withdrawMoney(double sum) throws Exception ;
	
	
}
