package model;

public class Time implements java.io.Serializable {
	
	private int minutes;
	private int hour;
	private int day;
	private int month;
	private int year;
	
	public Time() {
		
		this.minutes = this.hour = this.day = this.month = this.year = 0;
	}
	
	public Time(int tHour, int tDay, int tMonth, int tYear, int tMinutes) {
		
		this.hour = tHour;
		this.day = tDay;
		this.month = tMonth;
		this.year = tYear;
		this.minutes = tMinutes;
	}
	
	public int getHour() {
		
		return hour;
	}
	
	public int getDay() {
		
		return day;
	}
	
	public int getMonth() {
		
		return month;
	}
	
	public int getYear() {
		
		return year;
	}
	
	public int getMinutes() {
		
		return minutes;
	}
	
	public String toString() {
		
		return "date: "  +  getDay() + "/" + getMonth() + "/" + getYear() + ", hour " + getHour();
	}
}
