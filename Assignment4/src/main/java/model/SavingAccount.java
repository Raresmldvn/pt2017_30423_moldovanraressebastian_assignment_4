package model;

import java.time.LocalDateTime;

public class SavingAccount extends Account{
	

	private static final long serialVersionUID = 1L;
	private int interestTime;
	private double withdrawnMoney = 0;
	
	public SavingAccount(int id, int interestTime) {
		
		super();
		this.setId(id);
		LocalDateTime time = LocalDateTime.now();
		int hour = time.getHour();
		int day = time.getDayOfMonth();
		int month = time.getMonthValue();
		int year = time.getYear();
		int minute = time.getMinute();
		this.setTime(new Time(hour, day, month, year, minute));
		this.interestTime = interestTime;
	}
	
	public SavingAccount(int accountId, double accountMoney, Time depositTime, int accountInterest) {
		
		super(accountId, accountMoney, depositTime);
		this.interestTime = accountInterest;
	}
	
	public double getWithdrawnMoney() {
		
		return withdrawnMoney;
	}

	@Override
	public void depositMoney(double newSum) throws Exception {
		
		//assert newSum>=1000;
		
		if(newSum<1000)
			throw new Exception("Sum not enough!");
		if(getMoney()!=0)
			throw new Exception("Sum is fixed!");
		LocalDateTime time = LocalDateTime.now();
		int hour = time.getHour();
		int day = time.getDayOfMonth();
		int month = time.getMonthValue();
		int year = time.getYear();
		int minute = time.getMinute();
		
		setMoney(newSum);
		setTime(new Time(hour, day, month, year, minute));
		
		assert getMoney()>1000&&getTime().getDay()>0&&getTime().getMonth()>0&&getTime().getMonth()<=12;
	}

	@Override
	public void withdrawMoney(double sum) throws Exception {
		
		
		LocalDateTime time = LocalDateTime.now();
		int hour = time.getHour();
		int day = time.getDayOfMonth();
		int month = time.getMonthValue();
		int year = time.getYear();
		int minute = time.getMinute();

		if(computeTimeDifference(getTime(), new Time(hour, day, month, year, minute))==0) {
			
			throw new Exception("You cannot withdraw the money yet!");
		} else if(sum!=this.getMoney()) {
			
			throw new Exception("You have to withdraw all the money!");
		} else {
		withdrawnMoney = getMoney() + (6.0/100.0)*computeTimeDifference(getTime(), new Time(hour, day, month, year, minute))*this.getMoney();
		setMoney(getMoney()-sum);
		setTime(new Time(hour, day, month, year, minute));
		}
		
		assert getMoney()==0&&getTime().getDay()>0&&getTime().getMonth()>0&&getTime().getMonth()<=12;
	}
	
	public int getInterest() {
		
		return interestTime;
	}
	
	public double getWithDrawnMoney() {
		
		return withdrawnMoney;
	}
	public int computeTimeDifference(Time t1, Time t2) {
		
		if(t2.getMinutes() - t1.getMinutes()>=interestTime) {
			
			return t2.getMinutes() - t1.getMinutes();
		}
		return 0;
	}
	
}
