package model;

import java.time.LocalDateTime;

public class SpendingAccount extends Account {
	

	public double withDrawnMoney;
	
	public SpendingAccount(int id) {
		LocalDateTime time = LocalDateTime.now();
		int hour = time.getHour();
		int day = time.getDayOfMonth();
		int month = time.getMonthValue();
		int year = time.getYear();
		int minute = time.getMinute();
		this.setTime(new Time(hour, day, month, year, minute));
		this.setId(id);
	}

	public SpendingAccount(int accountId, double accountMoney, Time depositTime) {
		
		super(accountId, accountMoney, depositTime);
	}
	
	/**
	 * Withdraws the necessary amount of money from account
	 * @param sum Represent the sum that needs to be subtracted from the current sum.
	 */
	@Override
	public void withdrawMoney(double sum) throws Exception {
		
		
		LocalDateTime time = LocalDateTime.now();
		int hour = time.getHour();
		int day = time.getDayOfMonth();
		int month = time.getMonthValue();
		int year = time.getYear();
		int minute = time.getMinute();
		setTime(new Time(hour, day, month, year, minute));
		if(sum>getMoney())
			throw new Exception("There is not enogh money for the transaction!");
		else
			this.setMoney(getMoney() - sum);
		withDrawnMoney = sum;
		
		assert getMoney()>0;
	}
	
	public double getWithdrawnMoney() {
		
		return withDrawnMoney;
	}
	@Override
	public void depositMoney(double sum) {
		
		assert sum>0;
		LocalDateTime time = LocalDateTime.now();
		int hour = time.getHour();
		int day = time.getDayOfMonth();
		int month = time.getMonthValue();
		int year = time.getYear();
		int minute = time.getMinute();
		setTime(new Time(hour, day, month, year, minute));
		this.setMoney(getMoney() + sum);
	}
	
	public int getInterest() {
		
		return 0;
	}
	
}
